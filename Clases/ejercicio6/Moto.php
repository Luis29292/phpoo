<?php  
include_once('transporte.php');

// Declaración de la clase hija o subclase Moto
class Moto extends Transporte {
    private $cilindrada;

    // Declaración del constructor
    public function __construct($nom, $vel, $com, $cil) {
        // Sobrescritura del constructor de la clase padre
        parent::__construct($nom, $vel, $com);
        $this->cilindrada = $cil;
    }

    // Declaración del método
    public function resumenMoto() {
        // Sobrescritura del método crear_ficha en la clase padre
        $mensaje = parent::crear_ficha();
        $mensaje .= '<tr>
            <td>Cilindrada:</td>
            <td>'. $this->cilindrada .'</td>                
        </tr>';
        return $mensaje;
    }
} 

//$mensaje = '';

if (!empty($_POST) && ($_POST['tipo_transporte'] == 'moto')) {
    $moto1 = new Moto('moto', '150', 'gasolina', '125cc');
    $mensaje = $moto1->resumenMoto();
}

?>

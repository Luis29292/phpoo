<?php
//creación de la clase carro
class Carro2{
	//declaracion de propiedades
	public $color;
	public $modelo;
	private $veri;
	//declaracion del método verificación
	public function verificacion($veri){
		$veri = intval(substr($veri, 0, 4)); // Esto devolverá solo el año y guardará la variable como entero
		switch (true) {
			case $veri < 1990:
				$this->veri = "No circula";
				break;
			case $veri >= 1990 && $veri <= 2010:
				$this->veri= "Revisión";
				break;
			case $veri > 2010:
				$this->veri= "Sí circula";
				break;
		}
		//$this->veri = substr($veri, 0, 4); // Esto devolverá solo el año
	}
	public function get_veri(){
		

		return $this->veri;
	}
}

//creación de instancia a la clase Carro
$Carro1 = new Carro2();

if (!empty($_POST)){
	$Carro1->color=$_POST['color'];
	$Carro1->modelo=$_POST['modelo'];
	//$Carro1->veri=$_POST['veri'];
	$Carro1->verificacion($_POST['veri']);
	//$Carro1->get_veri();
}




